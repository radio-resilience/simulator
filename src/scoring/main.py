import zmq
import time
import multiprocessing
import argparse
import logging
import sys
import signal
import json
import os

from rrc_scoring.dummy_radio import DummyRadio
from rrc_scoring.match_runner import MatchRunner

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
log = logging.getLogger("MatchController")
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
log.addHandler(handler)

def interrupt_handler(signal, frame):
  log.fatal("Caught Ctrl+C, exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, interrupt_handler)

if __name__ == '__main__':

  abspath = os.path.abspath(__file__)
  dname = os.path.dirname(abspath)
  os.chdir(dname)

  parser = argparse.ArgumentParser()
  parser.add_argument('-t', '--test', help="run in test mode with dummy radio", action="store_true")
  parser.add_argument('-S', '--src_ip', type=str, help="traffic source IP", default="127.0.0.1")
  parser.add_argument('-P', '--src_port', type=int, help="traffic source port", default=44001)
  parser.add_argument('-s', '--sink_ip', type=str, help="traffic sink IP", default="127.0.0.1")
  parser.add_argument('-p', '--sink_port', type=int, help="traffic sink port", default=44002)
  parser.add_argument('-i', '--iq_sink_ip', type=str, help="IQ sink IP", default="muxer")
  parser.add_argument('-I', '--iq_sink_port', type=int, help="IQ sink port", default=33008)
  parser.add_argument('-c', '--config', type=str, help="JSON match config file", default="match_config.json")

  args = parser.parse_args()

  if args.test:
    dr = DummyRadio(args.src_ip, args.src_port, args.sink_ip, args.sink_port)
    dr.run()

  config = json.loads(open(args.config, 'r').read())
  print(config)

  mr = MatchRunner(args.src_ip, args.src_port, args.sink_ip, args.sink_port, args.iq_sink_ip, args.iq_sink_port, config)
  mr.run()

  log.critical("Done.")

  #sys.exit(0)

