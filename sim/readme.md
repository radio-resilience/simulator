## IQ muxer with two client nodes

### Containers

#### grc1
GRC dev container #1 

#### grc2
GRC dev container #2 

#### muxer
simple muxer 

### Usage

Each GRC dev container sends its TX IQ to muxer using a ZMQ push block, and receives the final channel IQ using a ZMQ sub block.

The muxer consumes IQ from both GRC dev containers using ZMQ pull blocks, attenuates and sums the signals, and then sends the IQ back to the GRC dev containers using a ZMQ pub block.

```
$ cd sim/env
$ ./run.sh
```
